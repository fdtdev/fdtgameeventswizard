# DEPRECATED

# FDT GameEvents Wizard

GameEventsWizard is an Editor Window that creates the boilerplate scripts necesary for custom GameEvent usage.


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.gameeventswizard": "https://bitbucket.org/fdtdev/fdtgameeventswizard.git#1.3.1",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtgameeventswizard/src/1.3.1/LICENSE.md)