﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.CodeDom.Compiler;
using System.Reflection;
using UnityEngine;
using UnityEditor;
using com.FDT.Common.Editor;

namespace com.FDT.Common.GameEventsWizard.Editor
{
    public class GameEventsWizard : EditorWindow
	{
		const float k_ScreenSizeWindowBuffer = 50f;
		const float k_WindowWidth = 500f;
		const float k_MaxWindowHeight = 800f;
		const int k_PlayableNameCharLimit = 64;
		const string k_folderSuffix = "GameEvent";

		public string playableName = "";
		public int numParameters = 1;
		public List<string> parameterNames = new List<string>();
		public List<UsableTypeSelection> parameterDatas = new List<UsableTypeSelection>();
		public List<string> parameterTypeNames = new List<string>();

        Vector2 m_ScrollViewPos;
		bool m_CreateButtonPressed;
		CreationError m_CreationError;

		readonly GUIContent m_PlayableNameContent = new GUIContent("GameEvent Name", "This is the name that will represent the GameEvent.  E.G. Entity.  It will be the basis for the class names so it is best not to use the postfixes: 'GameEvent', 'Listener', 'Drawer', 'Handle' or 'Editor'.");
		readonly GUIContent m_NumParamsContent = new GUIContent ("Parameters number", "Number of parameters. To use no parameters, use the base GameEvent as is.");

		public static string gameEventsNameSpace = "com.FDT.Common.GameEvents";

		public static string nameSpaceVar { get { return Application.productName + "_GameEventsWizard"; } }
		[MenuItem("Window/GameEvents Wizard...")]
		static void CreateWindow ()
		{
            GameEventsWizard wizard = GetWindow<GameEventsWizard>(true, "GameEvents Wizard", true);

			if (EditorPrefs.HasKey (nameSpaceVar)) {
				gameEventsNameSpace = EditorPrefs.GetString (nameSpaceVar);
			} else {
				EditorPrefs.SetString (nameSpaceVar, gameEventsNameSpace);
			}

			Vector2 position = Vector2.zero;
			SceneView sceneView = SceneView.lastActiveSceneView;
			if (sceneView != null)
				position = new Vector2(sceneView.position.x, sceneView.position.y);
			wizard.position = new Rect(position.x + k_ScreenSizeWindowBuffer, position.y + k_ScreenSizeWindowBuffer, k_WindowWidth, Mathf.Min(Screen.currentResolution.height - k_ScreenSizeWindowBuffer, k_MaxWindowHeight));

			wizard.Show();

			Init ();
		}
		static UsableTypes usableTypes = new UsableTypes();

		static void Init ()
		{
			if (UsableTypes.usableTypes.Count == 0)
				usableTypes.Init ();
		}

		readonly GUIContent m_TrackBindingTypeContent = new GUIContent("Track Binding Type", "This is the type of object the Playable will affect.  E.G. To affect the position choose Transform.");

		void OnGUI ()
		{
			if (UsableTypes.usableTypes.Count == 0)
				Init ();
			m_ScrollViewPos = EditorGUILayout.BeginScrollView (m_ScrollViewPos);

			EditorGUILayout.HelpBox ("This wizard is used to create a Custom GameEvent type. "
				+ "It will create 6 ready to use scripts. You shouldn't need anything"
				+ " else to use a Custom GameEvent.", MessageType.None);
			EditorGUILayout.Space ();

			EditorGUILayout.BeginVertical (GUI.skin.box);
			EditorGUI.BeginChangeCheck ();
			gameEventsNameSpace = EditorGUILayout.TextField ("Namespace ", gameEventsNameSpace);
			if (EditorGUI.EndChangeCheck ()) {
				EditorPrefs.SetString (nameSpaceVar, gameEventsNameSpace);
			}

			playableName = EditorGUILayout.TextField (m_PlayableNameContent, playableName);

			bool playableNameNotEmpty = !string.IsNullOrEmpty (playableName);
			bool playableNameFormatted = CodeGenerator.IsValidLanguageIndependentIdentifier(playableName);
			if (!playableNameNotEmpty || !playableNameFormatted)
			{
				EditorGUILayout.HelpBox ("The GameEvent needs a name which starts with a capital letter and contains no spaces or special characters.", MessageType.Error);
			}
			bool playableNameTooLong = playableName.Length > k_PlayableNameCharLimit;
			if (playableNameTooLong)
			{
				EditorGUILayout.HelpBox ("The GameEvent needs a name which is fewer than " + k_PlayableNameCharLimit + " characters long.", MessageType.Error);
			}

			numParameters = EditorGUILayout.IntSlider (m_NumParamsContent, numParameters, 1, 4);

			while (parameterNames.Count < numParameters) {
				parameterNames.Add (string.Empty);
			}
			while (parameterDatas.Count < numParameters) {
				parameterDatas.Add (new UsableTypeSelection ());
			}

			for (int i = 0; i < numParameters; i++)
			{
				EditorGUILayout.BeginHorizontal ();
                AskVariable(i);
				EditorGUILayout.EndHorizontal ();
			}

			EditorGUILayout.EndVertical ();
			EditorGUILayout.Space();
			EditorGUILayout.Space();


			if (playableNameNotEmpty && playableNameFormatted /*&& allUniqueVariableNames && exposedVariablesNamesValid && scriptVariablesNamesValid*/ && !playableNameTooLong)
			{
				if (GUILayout.Button("Create", GUILayout.Width(60f)))
				{	
					parameterTypeNames.Clear ();
					foreach (var p in parameterDatas) {
						parameterTypeNames.Add (p.value.name);
					}
					m_CreateButtonPressed = true;
					m_CreationError = CreateScripts();
				}
			}

			EditorGUILayout.Space();
			EditorGUILayout.Space();

			if (m_CreateButtonPressed)
			{
				switch (m_CreationError)
				{
				case CreationError.NoError:
					EditorGUILayout.HelpBox ("Playable was successfully created.", MessageType.Info);
					break;
				case CreationError.CustomEventListenerAlreadyExists:
					EditorGUILayout.HelpBox ("The type " + playableName + k_GameEventListenerSuffix + " already exists, no files were created.", MessageType.Error);
					break;
				case CreationError.CustomGameEventAlreadyExists:
					EditorGUILayout.HelpBox ("The type " + playableName + k_GameEventSuffix + " already exists, no files were created.", MessageType.Error);
					break;
				case CreationError.CustomGameEventEditorAlreadyExists:
					EditorGUILayout.HelpBox ("The type " + playableName + k_GameEventEditorSuffix + " already exists, no files were created.", MessageType.Error);
					break;				
				}
			}

            EditorGUILayout.EndScrollView ();
		}
        protected void AskVariable(int idx)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Class: ", GUILayout.Width(40));
            EditorGUI.BeginChangeCheck();
            parameterDatas[idx].currText = EditorGUILayout.TextField(GUIContent.none, parameterDatas[idx].currText, GUILayout.MinWidth(30), GUILayout.MaxWidth(200));
            if (EditorGUI.EndChangeCheck())
                parameterDatas[idx].GetEnumList();
            List<UsableTypeSelection.EnumStringValuePair> enumList = parameterDatas[idx].enumList;
            List<GUIContent> enumStrList = new List<GUIContent>(enumList.Count);
            for (int i = 0; i < enumList.Count; ++i)
            {
                enumStrList.Add(enumList[i].strValue.guiContent);
            }
            int selectedIndex = 0;
            for (int i = 0; i < enumList.Count; ++i)
            {
                if (enumList[i].intValue == parameterDatas[idx].enumValueIndex)
                {
                    selectedIndex = i;
                    break;
                }
            }
            selectedIndex = EditorGUILayout.Popup(GUIContent.none, selectedIndex, enumStrList.ToArray(), GUILayout.MinWidth(30), GUILayout.MaxWidth(200));
            if (enumList.Count > selectedIndex)
            {
                parameterDatas[idx].enumValueIndex = enumList[selectedIndex].intValue;
                parameterDatas[idx].value = enumList[selectedIndex].strValue;
            }
            EditorGUILayout.LabelField( "PropName: ", GUILayout.Width(70));
            parameterNames[idx] = EditorGUILayout.TextField(GUIContent.none, parameterNames[idx], GUILayout.MinWidth(30), GUILayout.MaxWidth(200));
            
            EditorGUILayout.EndHorizontal();
        }        
		protected string GetCurrentPath()
		{
			var path = "Assets";
			var obj = Selection.activeObject;
			if (obj != null)
			{
				path = AssetDatabase.GetAssetPath (obj.GetInstanceID ());
				if (path.Length > 0) 
				{
					if (Directory.Exists (path)) 
					{
						return path;

					}
				}
			}
			return "Assets";
		}

		const string k_GameEventSuffix = "GameEvent";
		const string k_GameEventListenerSuffix = "GameEventListener";
		const string k_GameEventEditorSuffix = "GameEventEditor";
		const string k_GameEventHandleSuffix = "GameEventHandle";
		const string k_GameEventHandleDrawerSuffix = "GameEventHandleDrawer";
		const string k_GameEventCallerSuffic = "GameEventCaller";
		
		/*const string k_GameEventBehaviourSuffix = "Behaviour";
		const string k_GameEventClipSuffix = "Clip";
		const string k_GameEventMixerBehaviourSuffix = "MixerBehaviour";
		const string k_GameEventTrackSuffix = "Track";
		const string k_GameEventBehaviourDrawerSuffix = "BehaviourDrawer";*/


		CreationError CreateScripts ()
		{
			string currentPath = GetCurrentPath ();
			string currentFullPath = Application.dataPath;
			currentFullPath = currentFullPath.Substring (0, currentFullPath.Length - 6) + currentPath;

			if (ScriptAlreadyExists(playableName + k_GameEventSuffix))
				return CreationError.CustomGameEventAlreadyExists;

			if (ScriptAlreadyExists(playableName + k_GameEventListenerSuffix))
				return CreationError.CustomEventListenerAlreadyExists;

			if (ScriptAlreadyExists(playableName + k_GameEventEditorSuffix))
				return CreationError.CustomGameEventEditorAlreadyExists;

			if (ScriptAlreadyExists (playableName + k_GameEventHandleSuffix))
				return CreationError.OtherError;

			if (ScriptAlreadyExists (playableName + k_GameEventHandleDrawerSuffix))
				return CreationError.OtherError;
			
			if (ScriptAlreadyExists (playableName + k_GameEventCallerSuffic))
				return CreationError.OtherError;

			AssetDatabase.CreateFolder (currentPath, playableName + k_folderSuffix);

			string scriptFullFolder = currentFullPath + "/" + playableName + k_folderSuffix;
			string scriptFolder = currentPath + "/" + playableName + k_folderSuffix;

			CreateScript (scriptFullFolder, playableName + k_GameEventSuffix, GetGameEventScript());
			CreateScript (scriptFullFolder, playableName + k_GameEventListenerSuffix, GetGameEventListenerScript ());
			CreateScript (scriptFullFolder, playableName + k_GameEventHandleSuffix, GetGameEventHandleScript ());
			CreateScript (scriptFullFolder, playableName + k_GameEventCallerSuffic, GetGameEventCallerScript());
			
			AssetDatabase.CreateFolder (scriptFolder, "Editor");

			string path =scriptFullFolder + "/Editor/" + playableName + k_GameEventEditorSuffix + ".cs";
			using (StreamWriter writer = File.CreateText (path))
			{
				writer.Write (GetGameEventEditorScript ());
			}

			path = scriptFullFolder + "/Editor/" + playableName + k_GameEventHandleDrawerSuffix + ".cs";
			using (StreamWriter writer = File.CreateText (path))
			{
				writer.Write (GetGameEventHandleDrawerScript ());
			}

			AssetDatabase.SaveAssets ();
			AssetDatabase.Refresh ();
            EditorExtensions.SetIcon(playableName + k_GameEventSuffix, "GameEvent Icon");
			return CreationError.NoError;
		}

		void CreateScript (string folderpath, string fileName, string content)
		{
			string path = folderpath + "/" + fileName + ".cs";
			using (StreamWriter writer = File.CreateText (path))
				writer.Write (content);
		}
		public enum CreationError
		{
			NoError,
			CustomGameEventAlreadyExists,
			CustomEventListenerAlreadyExists,
			CustomGameEventEditorAlreadyExists,
			OtherError,
		}
		bool ScriptAlreadyExists(string scriptName)
		{
			string[] guids = AssetDatabase.FindAssets(scriptName);

            if (guids.Length == 0)
				return false;

			for (int i = 0; i < guids.Length; i++)
			{
				string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                string fileName = Path.GetFileName(path);
                if (fileName.StartsWith(scriptName))
                {
                    Type assetType = AssetDatabase.GetMainAssetTypeAtPath(path);
                    if (assetType == typeof(MonoScript))
                        return true;
                }
            }

			return false;
		}
		
		string GetGameEventListenerScript()
		{
			string evtParams = parameterTypeNames [0] + " " + parameterNames [0];
			string uevtParams = parameterNames [0];
			string uevtTypeParams = parameterTypeNames [0];
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+	parameterTypeNames [i] + " " + parameterNames [i];
					uevtParams+=", "+ parameterNames [i];
					uevtTypeParams+=", "+ parameterTypeNames [i];
				}
			}
            
            string extension = null;
            string extension2 = null;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1Listener<{0}, {1}GameEvent, {2}GameEventListener.UEvt>",
	                    parameterTypeNames[0], playableName, playableName);
                    extension2 = string.Format("UnityEvent<{0}>\n", parameterTypeNames[0]);
                    break;
                case 2:
                    extension = string.Format("GameEvent2Listener<{0}, {1}, {2}GameEvent, {3}GameEventListener.UEvt>",
	                    parameterTypeNames[0], parameterTypeNames[1], playableName, playableName);
                    extension2 = string.Format("UnityEvent<{0}, {1}>\n", parameterTypeNames[0], parameterTypeNames[1]);
                    break;
                case 3:
                    extension = string.Format(
	                    "GameEvent3Listener<{0}, {1}, {2}, {3}GameEvent, {4}GameEventListener.UEvt>",
	                    parameterTypeNames[0], parameterTypeNames[1], parameterTypeNames[2], playableName,
	                    playableName);
                    extension2 = string.Format("UnityEvent<{0}, {1}, {2}>\n", parameterTypeNames[0],
	                    parameterTypeNames[1], parameterTypeNames[2]);
                    break;
                case 4:
                    extension = string.Format(
	                    "GameEvent4Listener<{0}, {1}, {2}, {3}, {4}GameEvent, {5}GameEventListener.UEvt>",
	                    parameterTypeNames[0], parameterTypeNames[1], parameterTypeNames[2], parameterTypeNames[3],
	                    playableName, playableName);
                    extension2 = string.Format("UnityEvent<{0}, {1}, {2}, {3}>\n", parameterTypeNames[0],
	                    parameterTypeNames[1], parameterTypeNames[2], parameterTypeNames[3]);
                    break;
            }

			return "using com.FDT.Common.GameEvents;\n" + 
                "using UnityEngine;\n"+
				"using UnityEngine.Events;\n"+
				AdditionalNamespacesToString() +
				"\n"+
				"namespace "+gameEventsNameSpace+"\n"+
				"{\n"+
				"\tpublic class "+playableName+"GameEventListener : "+extension+"\n"+
				"\t{\n"+
                "\t\t[System.Serializable]\n"+
                "\t\tpublic class UEvt : "+extension2 +
                "\t\t{}\n" +
				"\t}\n"+
				"}\n";
		}

		string GetGameEventEditorScript()
		{
			string evtParams = "_cTarget._editor_" + parameterNames [0];
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", _cTarget._editor_"+	parameterNames [i];			
				}
			}

            string extension = null;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1Editor<{0}>\n", parameterTypeNames[0]);
                    break;
                case 2:
                    extension = string.Format("GameEvent2Editor<{0}, {1}>\n", parameterTypeNames[0],
	                    parameterTypeNames[1]);
                    break;
                case 3:
                    extension = string.Format("GameEvent3Editor<{0}, {1}, {2}>\n", parameterTypeNames[0],
	                    parameterTypeNames[1], parameterTypeNames[2]);
                    break;
                case 4:
                    extension = string.Format("GameEvent4Editor<{0}, {1}, {2}, {3}>\n", parameterTypeNames[0],
	                    parameterTypeNames[1], parameterTypeNames[2], parameterTypeNames[3]);
                    break;
            }
            
			return "using UnityEditor;\n"+
				"using UnityEngine.Events;\n"+
                "using com.FDT.Common.GameEvents.Editor;\n"+
				"using "+gameEventsNameSpace+";\n"+
				AdditionalNamespacesToString() +
				"\n"+
				"namespace "+gameEventsNameSpace+".Editor\n"+
				"{\n"+
				"\t[CustomEditor(typeof("+playableName+"GameEvent))]\n"+
				"\tpublic class "+playableName+ "GameEventEditor : "+ extension +
				"\t{\n"+
				"\t}\n"+
				"}\n";
		}

		string GetGameEventCallerScript()
		{
			string evtParams = parameterTypeNames [0] + " " + parameterNames [0];
			string uevtParams = parameterTypeNames [0];
			string uevtCalls = parameterNames [0];
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+	parameterTypeNames [i] + " " + parameterNames [i];
					uevtParams+=", "+ parameterTypeNames [i];
					uevtCalls+=", " + parameterNames [i];
				}
			}
			string editorEvtParams = "[Space] public "+parameterTypeNames [0] + " " + parameterNames [0]+";\n";
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+	parameterTypeNames [i] + " " + parameterNames [i];
					uevtParams+=", "+ parameterNames [i];
					editorEvtParams+="\t\tpublic "+parameterTypeNames [i] + " " + parameterNames [i]+";\n";
				}
			}
            
            string extension = null;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1Caller<{0}, {1}GameEvent>", parameterTypeNames[0],
	                    playableName);
                    break;
                case 2:
                    extension = string.Format("GameEvent2Caller<{0}, {1}, {2}GameEvent>", parameterTypeNames[0],
	                    parameterTypeNames[1], playableName);
                    break;
                case 3:
                    extension = string.Format("GameEvent3Caller<{0}, {1}, {2}, {3}GameEvent>", parameterTypeNames[0],
	                    parameterTypeNames[1], parameterTypeNames[2], playableName);
                    break;
                case 4:
                    extension = string.Format("GameEvent4Caller<{0}, {1}, {2}, {3}, {4}GameEvent>",
	                    parameterTypeNames[0], parameterTypeNames[1], parameterTypeNames[2], parameterTypeNames[3],
	                    playableName);
                    break;
            }
            
			return 	"using com.FDT.Common.GameEvents;\n" + 
			        AdditionalNamespacesToString() +
					"\nnamespace "+gameEventsNameSpace+"\n{\n" +
					"\tpublic class "+playableName+"GameEventCaller: "+extension+"\n" +
					"\t{\n"+
					"\t}\n" +
					"}\n";
		}
		string GetGameEventHandleScript()
		{
			string evtParams = parameterTypeNames [0] + " " + parameterNames [0];
			string uevtParams = parameterTypeNames [0];
			string uevtCalls = parameterNames [0];
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+	parameterTypeNames [i] + " " + parameterNames [i];
					uevtParams+=", "+ parameterTypeNames [i];
					uevtCalls+=", " + parameterNames [i];
				}
			}
 
            string extension = null;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1Handle<{0}, {1}GameEvent>", parameterTypeNames[0],
	                    playableName);
                    break;
                case 2:
                    extension = string.Format("GameEvent2Handle<{0}, {1}, {2}GameEvent>", parameterTypeNames[0],
	                    parameterTypeNames[1], playableName);
                    break;
                case 3:
                    extension = string.Format("GameEvent3Handle<{0}, {1}, {2}, {3}GameEvent>", parameterTypeNames[0],
	                    parameterTypeNames[1], parameterTypeNames[2], playableName);
                    break;
                case 4:
                    extension = string.Format("GameEvent4Handle<{0}, {1}, {2}, {3}, {4}GameEvent>",
	                    parameterTypeNames[0], parameterTypeNames[1], parameterTypeNames[2], parameterTypeNames[3],
	                    playableName);
                    break;
            }
            
            return "using com.FDT.Common.GameEvents;\n" +
            AdditionalNamespacesToString() +
				"\nnamespace "+gameEventsNameSpace+"\n{\n" +
			"\t[System.Serializable]\n" +
			"\tpublic class "+playableName+"GameEventHandle: "+extension+"\n" +
			"\t{\n" +
			"\t}\n" +
			"}\n";
		}

		string GetGameEventScript ()
		{
			string evtParams = parameterTypeNames [0] + " " + parameterNames [0];

            if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+	parameterTypeNames [i] + " " + parameterNames [i];
				}
			}

            string extension = null;
            string paramsData = string.Empty;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1<{0}>", parameterTypeNames[0]);
                    break;
                case 2:
                    extension = string.Format("GameEvent2<{0}, {1}>", parameterTypeNames[0], parameterTypeNames[1]);
                    break;
                case 3:
                    extension = string.Format("GameEvent3<{0}, {1}, {2}>", parameterTypeNames[0], parameterTypeNames[1],
	                    parameterTypeNames[2]);
                    break;
                case 4:
                    extension = string.Format("GameEvent4<{0}, {1}, {2}, {3}>", parameterTypeNames[0],
	                    parameterTypeNames[1], parameterTypeNames[2], parameterTypeNames[3]);
                    break;
            }
            
            if (!string.IsNullOrEmpty(parameterNames[0]))
            {
                paramsData += "\t\tpublic override string arg0label { get { return \""+ parameterNames[0] + "\"; } }\n"; 
            }

            if (numParameters > 1)
            {
                if (!string.IsNullOrEmpty(parameterNames[1]))
                {
                    paramsData += "\t\tpublic override string arg1label { get { return \""+ parameterNames[1] + "\"; } }\n"; 
                }
            }
            if (numParameters > 2)
            {
                if (!string.IsNullOrEmpty(parameterNames[2]))
                {
                    paramsData += "\t\tpublic override string arg2label { get { return \""+ parameterNames[2] + "\"; } }\n"; 
                }
            }
            if (numParameters > 3)
            {
                if (!string.IsNullOrEmpty(parameterNames[3]))
                {
                    paramsData += "\t\tpublic override string arg3label { get { return \""+ parameterNames[3] + "\"; } }\n"; 
                }
            }
            return
				"using UnityEngine;\n"+
                "using com.FDT.Common.GameEvents;\n" +
            AdditionalNamespacesToString() +
				"\nnamespace "+gameEventsNameSpace+"\n" +
				"{\n" +
				"\t[CreateAssetMenu(menuName = \"GameEvents/"+playableName+"GameEvent\")]\n" +
				"\tpublic class "+playableName+ "GameEvent : "+extension+"\n" +
				"\t{\n" +
                paramsData +
				"\t}\n" +
				"}\n";
		}
		string GetGameEventHandleDrawerScript()
		{
			return "using com.FDT.Common.GameEvents.Editor;\n" + 
                "using UnityEngine;\n" +
			"using UnityEditor;\n" +
				"using "+gameEventsNameSpace+";\n\n" +
				"namespace "+gameEventsNameSpace+".Editor\n{\n" +
			"\t[CustomPropertyDrawer(typeof("+playableName+"GameEventHandle))]\n" +
			"\tpublic class "+playableName+"GameEventHandleDrawer : GameEventHandlesDrawer\n\t{\n" +
			"\t}\n}\n";
		}
		
		string AdditionalNamespacesToString ()
		{
			UsableTypes.UsableType[] allUsedTypes = new UsableTypes.UsableType[numParameters];
			for (int i = 0; i < numParameters; i++)
			{
				allUsedTypes [i] = parameterDatas [i].value;
			}

			string[] distinctNamespaces = UsableTypes.UsableType.GetDistinctAdditionalNamespaces (allUsedTypes).Where (x => !string.IsNullOrEmpty (x)).ToArray ();
			string returnVal = "";
			for (int i = 0; i < distinctNamespaces.Length; i++)
			{
				returnVal += "using " + distinctNamespaces[i] + ";\n";
			}
			return returnVal;
		}
	}
	public class UsableTypeSelection
	{
		public string currText = string.Empty;
		public UsableTypes.UsableType value = null;
		public List<UsableTypes.UsableType> dropdownList = new List<UsableTypes.UsableType>();
		public int enumValueIndex = -1;
		public List<UsableTypeSelection.EnumStringValuePair> enumList = new List<EnumStringValuePair>();

		public void GetEnumList()
		{
            if (string.IsNullOrEmpty(currText))
            {
                enumList.Clear();
                return;
            }
			List<EnumStringValuePair> allList = new List<EnumStringValuePair>();
			var enumValues = UsableTypes.usableTypes;

			for (int i = 0; i < enumValues.Count; ++i)
			{
				EnumStringValuePair pair = new EnumStringValuePair();
				pair.strValue = enumValues [i];
				pair.intValue = i;

				allList.Add(pair);
			}

			List<EnumStringValuePair> ret = new List<EnumStringValuePair>();
            for (int i = 0; i < allList.Count; ++i)
            {
                if (allList[i].strValue.name.StartsWith(currText, StringComparison.InvariantCultureIgnoreCase))
                {
                    ret.Add(allList[i]);
                }
            }
            enumList = ret;
		}

		public struct EnumStringValuePair : IComparable<EnumStringValuePair>
		{
			public UsableTypes.UsableType strValue;
			public int intValue;

			public int CompareTo(EnumStringValuePair another)
			{
				if (intValue < another.intValue)
					return -1;
				else if (intValue > another.intValue)
					return 1;
				return 0;
			}
		}
	}
}